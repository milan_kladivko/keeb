
# Using QMK for Our Minidox Keyboard

For editing, use the [qmk configurator](config.qmk.fm). The output of that is a json file with key code arrays. 

This JSON file is then compiled into a `keymap.c` file that is then used by QMK in a `make` clusterfuck. This needs to be done correctly due to the way it's supposed to be built.


## Navigating the Clusterfuck

**We have to play by QMK rules.** That is, the following files need to be in a cloned QMK repository, within its `qmk_firmware/keyboard` folder structure, otherwise its `python` magic fails in weird ways.

### Installing QMK

[
See if you did it right, if you're having issues when doing the later steps.
](https://docs.qmk.fm/#/newbs_getting_started)

Running `qmk setup` to deal with python and its dependencies or `git pull` in case submodules are out of date somehow. Try to be updated as much as possible.

## Compiling and Flashing

To convert the `.json` file into the `keymap.c` source file, from the subdirectory of qmk `qmk_firmware/keyboards/minidox/keymaps/mikl/`, run the following: 

```sh
qmk json2c -o keymap.c export.json
```

Then, we'll compile+flash in a single step by running the following: 
```sh
qmk flash
```

This will read the `keymap.c` file and any other **configuration files** in the same directory, namely 
 - `config.h` \
   header file variables in `#ifdef` and `#define` statements; right-vs-left plugged in or mouse speed settings
 - and `rules.mk` \
   setting variables for the compilation's makefile; not everything is turned on by default because of space constrants on the chips.

> Note: There might be a `.sh` file in the future for compiling, if I'd be so inclined to write it.


# Issues? 

When in doubt, `qmk doctor` and fix every warning.

## Linux udev rules | "Waiting for /dev/ttyACM0 to become writable"

Gotta allow some magic for linux perms. You could `sudo` every time,
but that'd be scuffed. QMK doctor shows this issue as a warning (dumb).

https://docs.qmk.fm/#/faq_build?id=linux-udev-rules
