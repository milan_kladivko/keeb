#include QMK_KEYBOARD_H


// layer order setting 
// (should be rearranged in the array, but it's optional)

#define __HOME__      0
#define __SYM__       1
#define __SYM_NUM__   4
#define __SYM_CZECH__ 7

#define __OS__    2
#define __MOUSE__ 8
#define __OS_SPECIAL__  3

#define __GAMING__       5
#define __GAMING_EXTRA__ 6

#define __LOCK__ 9


// aliases for frequently used keys 
#define ____    KC_NO
#define OOOO    KC_TRNS
#define _vVVv_  TO(__HOME__)


#define undo  RCTL(KC_Z)
#define cut   RCTL(KC_X)
#define copy  RCTL(KC_C)
#define paste RCTL(KC_V)
#define save  RCTL(KC_S)


// czech layer shorthands
#define hacek LSFT(KC_EQL)
#define carka KC_EQL
#define g_kase KC_NUHS

#define aaa   KC_8
#define rrr   KC_5
#define sss KC_3
#define e_h KC_2
#define e_c KC_0
#define u_c KC_LBRC
#define iii KC_9
#define yyy KC_7
#define u_k KC_SCLN
#define ccc KC_4
#define zzz KC_6


// symbol shorthands
// (specific to an underlying czech layout)
#define comma   KC_COMM
#define dot     KC_DOT
#define plus    KC_1
#define minus   KC_SLSH
#define comma   KC_COMM
#define star    RALT(KC_8)
#define equ     KC_MINS
#define undsc   KC_QUES
#define semic   KC_GRV

#define dont    LSFT(KC_NUHS)
#define quot    KC_COLN
#define bquot   RALT(KC_GRV)

#define slash   KC_LCBR
#define bslash  RALT(KC_BSLS)

#define par_l   RALT(KC_9)
#define par_r   RALT(KC_0)
#define curly_l RALT(KC_LCBR)
#define curly_r RALT(KC_RCBR)
#define brak_l  RALT(KC_LBRC)
#define brak_r  RALT(KC_RBRC)
#define htm_l   RALT(KC_COMM)
#define htm_r   RALT(KC_DOT)

#define pipe    RALT(KC_PIPE)
#define colon   KC_GT
#define ques    KC_LT

#define dollar  RALT(KC_4)
#define amp     RALT(KC_7)
#define at      RALT(KC_2)
#define argh    RALT(KC_1)
#define squig   RALT(KC_TILD)
#define perc    RALT(KC_5)
#define hasht   RALT(KC_3)
#define chev    RALT(KC_6)


// HOME ROW MODIFIERS
// https://precondition.github.io/home-row-mods

#define GU_A LGUI_T(KC_A)
#define AL_R LALT_T(KC_R)
#define SH_S LSFT_T(KC_S)
#define CT_T RCTL_T(KC_T)

#define CT_N RCTL_T(KC_N)
#define SH_E RSFT_T(KC_E)
#define AL_I LALT_T(KC_I)
#define GU_O RGUI_T(KC_O)

// If we have the #define set correctly, we can do per-key 
// instead of global setting for tapping term. 
// The global is shared to LT() calls sometimes,
// so it's good to set the home row separately from that. 
// You can also use the `TAPPING_TERM` const to set things relative
// to the global setting. 
uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    bool gottem = false;
    uint16_t home[8] = {
        GU_A, AL_R, SH_S, CT_T, 
        CT_N, SH_E, AL_I, GU_O,
    };
    for (int i = 0; i < 8; i++) {
        if (keycode == home[i]) {
            gottem = true;
        }
    }
    if (gottem) {
        return TAPPING_TERM * 3;
    } else {
        return TAPPING_TERM;
    }

    
    switch (keycode) {
        case SFT_T(KC_SPC):
            return TAPPING_TERM + 1250;
        case LT(1, KC_GRV):
            return 130;
        default:
            return TAPPING_TERM;
    }
};

// Kind of like home row modifiers, I use switching to the OS layer. 
// This doesn't use the same mechanism and doesn't respects 
// some mechanism to prevent mismatch when rolling. 
// Use only on keys that aren't often rolled with other keys. 
#define OS_T(kc) LT(__OS__, kc)

// MACROS 
// https://github.com/mugika9/qmk/blob/master/docs/macros.md
//   I() change interval of stroke in milliseconds.
//   D() press key.
//   U() release key.
//   T() type key(press and release).
//   W() wait (milliseconds).
//   END end mark.
#define CODEBLK 0
const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt) {
	if (record->event.pressed) {

    // NOTE:  Due to how macros are implemented in QMK (C macros adding `KC_`
    //   to the argument), you cannot use shorthands of other defines.
    //   No RALT, no switching layers, no symbol aliases. 

		switch(id) {
			case CODEBLK:
				return MACRO(  
          D(RALT), D(RSFT),T(LBRC), U(RSFT), U(RALT), // {
          T(ENTER), 
          W(100),     // For laggy IDEs, I need to not be pressing
          D(RALT), D(RSFT),T(RBRC), U(RSFT), U(RALT), // }
          W(100),     // at super-human speed -- it won't keep up. 
          T(UP), T(END), T(ENTER),
          END);
		}
	}
	return MACRO_NONE;
};


//
// Setting up the layer keys 
//
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [__HOME__] = LAYOUT_split_3x5_3(

    TO(__OS__), OS_T(KC_W), OS_T(KC_F), KC_P, KC_B,  /**/  KC_J, KC_L, KC_U, KC_Y, KC_Q, 
          GU_A,       AL_R,       SH_S, CT_T, KC_G,  /**/  KC_M, CT_N, SH_E, AL_I, GU_O, 
          KC_Z,       KC_X,       KC_C, KC_D, KC_V,  /**/  KC_K, KC_H, dot,  dont, ____,

    ____, OSM(MOD_LSFT), OSL(__SYM__),  /**/  KC_BSPC, KC_SPC, OSL(__SYM_CZECH__)
  ),

  [__SYM_CZECH__] = LAYOUT_split_3x5_3(

    hacek, g_kase, g_kase, hacek, hacek,  /**/  carka, u_c,   u_k,    yyy,    carka, 
    aaa,   rrr,    sss,   KC_T,  hacek,  /**/  carka, e_c,   e_h,    iii,    KC_O,
    zzz,   _vVVv_, ccc,   KC_D,  hacek,  /**/  carka, carka, _vVVv_, _vVVv_, _vVVv_, 

    OSM(MOD_LSFT), _vVVv_, _vVVv_,  /**/  _vVVv_, KC_SPC, _vVVv_
  ),

  // 

  [__OS__] = LAYOUT_split_3x5_3(

    _vVVv_,  save,    save,    KC_ESC,  KC_ESC,  /**/  KC_DEL,  KC_HOME, KC_UP,   KC_END,        M(CODEBLK), 
    KC_LGUI, KC_LALT, KC_LSFT, KC_RCTL, KC_ENT,  /**/  KC_BSPC, KC_LEFT, KC_DOWN, KC_RGHT,       undo,
    _vVVv_,  cut,     copy,    KC_TAB,  paste,   /**/  KC_SPC,  KC_TAB,  KC_ENT,  TO(__MOUSE__), _vVVv_, 

    _vVVv_, MO(__OS_SPECIAL__), TO(__MOUSE__),  /**/  KC_BSPC, KC_SPC, _vVVv_
  ),

  [__MOUSE__] = LAYOUT_split_3x5_3(

    _vVVv_,  save,    KC_ACL0, KC_ACL2, KC_ESC, /**/  KC_DEL,  KC_WH_D, KC_MS_U, KC_WH_U,    M(CODEBLK), 
    KC_LGUI, KC_LALT, KC_LSFT, KC_RCTL, KC_ENT, /**/  KC_BSPC, KC_MS_L, KC_MS_D, KC_MS_R,    undo, 
    _vVVv_,  cut,     copy,    KC_TAB,  paste,  /**/  KC_SPC,  KC_TAB,  KC_TAB,  TO(__OS__), _vVVv_, 
 
    _vVVv_, MO(__OS_SPECIAL__), TO(__OS__),  /**/  KC_BTN2, KC_BTN1, KC_BTN3
  ),

  [__OS_SPECIAL__] = LAYOUT_split_3x5_3(

    KC_F1,  KC_F2,   KC_F3,   KC_F4,       KC_F5,    /**/  KC_F6,   KC_F7,   KC_F8,   KC_F9, KC_F10, 
    RESET,  KC_VOLD, KC_VOLU, KC_F11,      KC_F12,   /**/  KC_INS,  KC_CAPS, CL_SWAP, ____,  ____, 
    _vVVv_, KC_BRID, KC_BRIU, LALT(KC_F4), BL_TOGG,  /**/  KC_NLCK, KC_LCTL, CL_NORM, ____,  KC_PSCR, 

    _vVVv_, OOOO, ____,  /**/  ____, OOOO, ____
  ),

  // 

  [__SYM__] = LAYOUT_split_3x5_3(

        dollar, comma, curly_l, curly_r,    at, /**/  amp,  bquot, dont,  quot,   ques, 
         semic, slash,   par_l,   par_r,  pipe, /**/  star, equ,   colon, bslash, undsc, 
    TO(__OS__),   dot,  brak_l,  brak_r, squig, /**/  plus, minus, htm_l, htm_r,  argh, 

    TO(__GAMING__), OOOO, ____,  /**/  KC_BSPC, LT(__SYM_NUM__,KC_SPC), ____
  ),

  [__SYM_NUM__] = LAYOUT_split_3x5_3(

    dollar, comma, curly_l, curly_r, dot,     /**/  plus,  LSFT(KC_7), LSFT(KC_8), LSFT(KC_9), dot, 
    perc,   hasht, par_l,   par_r,   KC_ENT,  /**/  minus, LSFT(KC_4), LSFT(KC_5), LSFT(KC_6), LSFT(KC_0), 
    _vVVv_, dot,   brak_l,  brak_r,  chev,    /**/  equ,   LSFT(KC_1), LSFT(KC_2), LSFT(KC_3), LSFT(KC_0), 

    ____, OOOO, ____,  /**/  ____, OOOO, OOOO
  ),

  //

  [__GAMING__] = LAYOUT_split_3x5_3(

    KC_TAB,  KC_Q, KC_W, KC_E, KC_R, /**/  ____, KC_WH_U, KC_MS_U, KC_WH_D, KC_ESC, 
    KC_LSFT, KC_A, KC_S, KC_D, KC_F, /**/  ____, KC_MS_L, KC_MS_D, KC_MS_R, KC_ACL2, 
    KC_LCTL, KC_Z, KC_X, KC_C, KC_V, /**/  ____, ____,    ____,    ____,    _vVVv_, 

    OSL(__GAMING_EXTRA__), KC_SPC, KC_BTN1,  /**/  KC_BTN2, KC_BTN1, KC_BTN3
  ),

  [__GAMING_EXTRA__] = LAYOUT_split_3x5_3(

    KC_6, KC_7,          KC_8,   KC_9,   KC_0,    /**/  ____, ____, ____, ____, ____, 
    KC_1, KC_2,          KC_3,   KC_4,   KC_5,    /**/  ____, ____, ____, ____, ____, 
    ____, OSM(MOD_LALT), KC_TAB, KC_ESC, KC_ENT,  /**/  ____, ____, ____, ____, ____, 

    OOOO, ____, _vVVv_,  /**/  ____, ____, ____
  ),

  // 

  [__LOCK__] = LAYOUT_split_3x5_3(

    // Tap dancing would do wonders here -- nothing else would probably work properly...
    ____, ____, ____, ____, ____,  /**/  ____, ____, ____, ____, ____, 
    ____, ____, ____, ____, ____,  /**/  ____, ____, ____, ____, ____, 
    ____, ____, ____, ____, ____,  /**/  ____, ____, ____, ____, ____, 

    OOOO, ____, _vVVv_,  /**/  ____, ____, ____
  ),

};
